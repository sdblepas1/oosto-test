# Oosto-test



Oosto Senior DevOps Assignment
We would like you to demonstrate your DevOps skills by completing the following tasks:
1. Write an Ansible playbook to create a Kubernetes master (all-in-one single node)
   on a remote server.
   The playbook should include all necessary prerequisites to set up a cluster.
   You can use whichever Kubernetes flavor you’d like (k3s, microk8s, vanilla etc.)
2. Once the Kubernetes cluster is created, use Helm to install Nginx webserver on
   the cluster.
3. Configure the Nginx webserver to display a custom message, which should be
   determined via an Ansible variable.
   Example: “Hello World! I’m a Senior DevOps Engineer candidate @ Oosto!”
4. Write a custom automation pipeline to execute the Ansible playbook on a remote
   server, and verify that the playbook completes successfully.
5. BONUS: Incorporating some form of infrastructure-as-code (IaC) tooling, such as
   Terraform, to create the remote server and execute the playbook.