# Terraform code to create an EC2 instance and execute Ansible playbook

provider "aws" {
  region = var.aws_region
}

resource "aws_instance" "ec2_instance" {
  ami           = var.aws_ami
  instance_type = var.aws_instance_type
  key_name      = var.aws_key_name
  security_groups = [var.aws_security_group]
  tags = {
    Name = "k3d-cluster"
  }
}

resource "null_resource" "ansible_provisioner" {
  depends_on = [aws_instance.ec2_instance]
  connection {
    type        = "ssh"
    user        = var.ssh_user
    private_key = file(var.ssh_private_key)
    host        = aws_instance.ec2_instance.public_ip
  }
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get -y install python3-pip",
      "sudo pip3 install ansible",
      "sudo ansible-galaxy install -r /ansible/requirements.yml",
      "sudo ansible-playbook /ansible/playbook.yml -i /ansible/inventory.ini --extra-vars 'nginx_custom_message=${var.nginx_custom_message}'"
    ]
  }
}
