# Variables for Terraform
aws_region = "us-west-2"
aws_ami = "ami-0c55b159cbfafe1f0"
aws_instance_type = "t2.micro"
aws_key_name = "my-key"
aws_security_group = "sg-1234567890"

# Variables for Ansible
ssh_user = "ubuntu"
ssh_private_key = "/path/to/my/key.pem"
